<?php

class Answer extends Illuminate\Database\Eloquent\Model
{
    public $guarded = array('id');

    public function question() {
        return $this->belongsTo('Question');
    }
}