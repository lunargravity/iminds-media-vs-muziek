<?php


class Question extends \Illuminate\Database\Eloquent\Model {
    public static function current($includeAnswers = false)
    {
        return Question::with('answers')->where('current', 1)->first();
    }

    public function toArray()
    {
        $data = parent::toArray();

        $myAnswer = Answer::where('question_id', $this->id)->where('answered_by', $_SERVER['REMOTE_ADDR'])->first();
        if($myAnswer != null)
        {
            $data['myAnswer'] = $myAnswer->answer;
        }

        $data['percentage_agree'] = $this->getPercentage('agree');
        $data['percentage_disagree'] = $this->getPercentage('disagree');
        $data['percentage_neutral'] = (100 - $data['percentage_disagree'] - $data['percentage_agree']);

        return $data;
    }

    public function getPercentage($result)
    {
        $totalAnswers = $this->answers()->count();
        $resultCount = 0;

        if($totalAnswers == 0) return 0;

        foreach($this->answers as $answer)
        {
            if($answer->answer == $result)
            {
                $resultCount++;
            }
        }

        return round((($resultCount / $totalAnswers) * 100), 1);
    }

    public function answers()
    {
        return $this->hasMany('Answer');
    }
}