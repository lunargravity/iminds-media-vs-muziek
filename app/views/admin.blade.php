@extends('main.presentation')
@section('content')
    <aside id="sidebar">
        <img id="arrow" src="/images/arrow.png">
    </aside>
    <section id="presentationContainer" class="admin">
        <h2>ADMIN</h2>
        @if($nextPhase == 'end')
            <h2>THE END</h2>
        @else
        <a class="button" id="next" href="/change?question={{ $nextQuestion }}&phase={{ $nextPhase }}">VOLGENDE</a>
        @endif


        @foreach($questions as $question)
        <div class="question">
            <h2>{{ $question->content }}</h2>
            <a class="button @if($currentQuestion == $question->id && $currentPhase == 'vote')current@endif" href="/change?question={{ $question->id }}&phase=vote">STEM-FASE</a>
            <a class="button @if($currentQuestion == $question->id && $currentPhase == 'wait')current@endif" href="/change?question={{ $question->id }}&phase=wait">WACHT-FASE</a>
            <a class="button @if($currentQuestion == $question->id && $currentPhase == 'result')current@endif" href="/change?question={{ $question->id }}&phase=result">RESULTAAT-FASE</a>

        </div>
        @endforeach

    </section>
@stop