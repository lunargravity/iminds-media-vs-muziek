@extends('main.presentation')
@section('content')
    <aside id="sidebar">
        <img id="arrow" src="/images/arrow.png">
    </aside>
    <section id="presentationContainer">
        <img src="/images/intro.png" id="intro" style="display: none" />
        <h1 id="question">"Als de media niet innoveert in Vlaanderen kijken we straks enkel naar buitenlandse content"</h1>
        <h1 id="end" style="display: none">Bedankt voor je deelname!</h1>
        <div id="answered">
            <a class="button agree">Eens</a>
            <a class="button neutral">Geen mening</a>
            <a class="button disagree">Oneens</a>
        </div>

        <div id="chart">
            <canvas id="canvas" height="400px" width="400px"></canvas>
            <img src="/images/shadow.png" width="400px" style="margin-top: 30px;margin-left: auto;margin-right: auto;" />

            <h3 class="label agree">Eens<br /><span id="agreeResult"></span></h3>
            <h3 class="label neutral">Geen mening<br /><span id="neutralResult"></span></h3>
            <h3 class="label disagree">Oneens<br /><span id="disagreeResult"></span></h3>
        </div>

        <p id="message" style="display: none">Selecteer wifi network mix2014!<br />Paswoord: mix2014!<br /><br />Ga naar mixapp.be om deel te nemen aan de stemming</p>


        <img id="logo" src="/images/logo.png">
    </section>

    <script>
        var current = {{ $currentQuestion }}
    </script>
@stop