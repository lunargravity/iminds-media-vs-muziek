<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta property="og:image" content="{{ SITE_URL }}/image_src.png">
        <meta property="og:title" content="@lang('app.title')">
        <meta property="og:site_name" content="@lang('app.title')">
        <meta property="og:description" content="@lang('app.description')">
        <meta property="og:url" content="{{ SITE_URL }}">
        <meta property="og:type" content="website">
        <link rel="shortcut icon" href="/favicon.ico">
        <link href="/stylesheets/screen.css" rel="stylesheet" type="text/css" />
        <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@lang('app.title')</title>
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-48089247-5', 'lunargravity.be');
            ga('send', 'pageview');

        </script>
    </head>
    <body @if(isset($hideCursor))style="cursor:none"@endif>