@include('main.head')
<div id="root">
    @yield('content')
</div>

@include('main.footer')