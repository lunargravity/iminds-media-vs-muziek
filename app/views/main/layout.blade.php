@include('main.head')
<div id="root">
    @yield('content')
    <div id="root-footer"></div>
</div>
<footer id="footer">
    <img id="logo" src="/images/logo.png">
</footer>

@include('main.footer')