@extends('main.layout')
@section('content')
    <header id="header">
        <h2 style="display: none">Je kan nu stemmen:</h2>
        <img src="/images/arrow_down.png" id="arrow" />
    </header>
    <section id="voteContainer">
        <h1>Als de media niet innoveert in Vlaanderen kijken we straks enkel naar buitenlandse content</h1>
        <div id="answers" style="display: none">
            <a href="#" data-answer="agree" class="button agree">Eens</a>
            <a href="#" data-answer="neutral" class="button neutral">Geen mening</a>
            <a href="#" data-answer="disagree" class="button disagree">Oneens</a>
        </div>
        <div id="answered" style="display: none">
            <p class="message" id="noAnswer">Geen antwoord gegeven</p>
            <a href="#" id="answer_agree" class="button answer agree">Eens</a>
            <a href="#" id="answer_neutral" class="button answer neutral">Geen mening</a>
            <a href="#" id="answer_disagree" class="button answer disagree">Oneens</a>
        </div>
        <div id="end" style="display: none;margin-bottom: 70px;margin-top:90px">
            <h2 style="color: #FFF; text-align: center">Gebruik zo dadelijk deze MiX App tijdens het concert van de Compact Disk Dummies. Even geduld.</h2>
        </div>
        <img src="/images/mobile_intro.png" id="intro" style="display: none" />
    </section>



    <script>
        var current = {{ $currentQuestion }}
    </script>
@stop