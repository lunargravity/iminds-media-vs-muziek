<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | App Language Lines
    |--------------------------------------------------------------------------
    |
    | App-specific locale
    |
    */

    'title' => 'Media vs Muziek',
    'description' => 'Het leven is al moeilijk genoeg zonder te moeten bijhouden waar al die obussen liggen.',
    'footer' => 'Brought to you by <a target="_blank" href="http://nl.wikipedia.org/wiki/Eerste_Wereldoorlog">WW1</a>, <a href="http://nl.wikipedia.org/wiki/Tweede_Wereldoorlog" target="_blank">WW2</a> & <a href="//lunargravity.com?utm_source=obusfindr">Lunar Gravity</a>'
);