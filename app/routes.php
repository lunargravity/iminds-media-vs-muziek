<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
    $current = Question::current();

    return View::make('vote')->with('currentQuestion', json_encode($current->toArray()));
});
Route::any('/vote', function()
{
    $current = Question::current();

    $uid = $_SERVER['REMOTE_ADDR'];

    $oldAnswered = Answer::where('answered_by', $uid)->where('question_id', $current->id)->first();
    if($oldAnswered != null)
    {
        return Response::json(array('answer' => $oldAnswered->answer));
    }

    $answer = new Answer();
    $answer->question_id = $current->id;
    $answer->answer = Input::get('answer');
    $answer->answered_by = $uid;

    $answer->save();

    return Response::json(array('answer' => $answer->answer));
});
Route::get('/presentation', function()
{
    $current = Question::current();
    return View::make('presentation')->with('currentQuestion', json_encode($current->toArray()))->with('hideCursor', true);
});
Route::get('/admin', function()
{
    $questions = Question::all();
    $currentQuestion = 1;
    $currentPhase = 'start';

    foreach($questions as $question)
    {
        if($question->current == 1)
        {
            $currentQuestion = $question->id;
            $currentPhase = $question->phase;
        }
    }

    switch($currentPhase)
    {
        case 'start':
            $nextPhase = 'vote';
            $nextQuestion = $currentQuestion;
            break;
        case 'vote':
            $nextPhase = 'wait';
            $nextQuestion = $currentQuestion;
            break;
        case 'wait':
            $nextPhase = 'result';
            $nextQuestion = $currentQuestion;
            break;
        case 'result':
            $nextPhase = 'vote';
            $nextQuestion = $currentQuestion + 1;
            break;
        case 'end':
            $nextPhase = 'end';
            $nextQuestion = $currentQuestion;
            break;
    }

    return View::make('admin')->with(array(
        'questions' => $questions,
        'nextQuestion' => $nextQuestion,
        'nextPhase' => $nextPhase,
        'currentQuestion' => $currentQuestion,
        'currentPhase' => $currentPhase
    ));
});
Route::get('/change', function()
{
    $phase = Input::get('phase');
    $question = Input::get('question');

    Question::query()->update(array('current' => 0));

    $nextQuestion = Question::where('id', $question);
    if($nextQuestion->count() != 0)
    {
        $nextQuestion->update(array('current' => 1, 'phase' => $phase));
    }
    else
    {
        Question::where('id', ($question - 1))->update(array('current' => 1, 'phase' => 'end'));
    }

    return Redirect::to('/admin');
});
Route::get('/update', function () {

    $question = Question::current();
    return Response::json($question->toArray());
});
Route::get('/reset', function () {

    Answer::query()->delete();
    Question::query()->update(array('current' => 0, 'phase' => 'vote'));
    Question::where('id', 1)->update(array('current' => 1, 'phase' => 'start'));

    return Redirect::to('/admin');
});


Route::controller('users', 'UserController');