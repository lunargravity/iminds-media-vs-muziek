jsApp = {
    currentQuestion: current,

    init: function () {
        if($('#canvas').length > 0) jsApp.initPresentation();
        if($('#answers').length > 0) jsApp.initVote();
    },

    initVote: function () {
        jsApp.updateVoteScreen();
        setInterval(jsApp.fetchCurrent, 2000);

        $('#answers .button').on('click', function (e) {
            e.preventDefault();

            var answerData = $(this).data('answer');

            $.ajax({
                url: '/vote',
                method: 'post',
                data: {
                    answer: answerData
                },
                success: function (result) {
                    jsApp.currentQuestion.myAnswer = result.answer;

                    jsApp.showAnswer();
                }
            })
        });
    },

    fetchCurrent: function() {

        var includeAnswers = ($('#canvas').length > 0);

        $.ajax({
            url: '/update',
            dataType: 'json',
            success: function(result) {
                if(jsApp.currentQuestion.id != result.id || jsApp.currentQuestion.phase != result.phase || jsApp.currentQuestion.myAnswer != result.myAnswer)
                {
                    jsApp.currentQuestion = result;
                    if($('#answers').length > 0) jsApp.updateVoteScreen();
                    if($('#canvas').length > 0) jsApp.updatePresentationScreen();
                }
            }
        })
    },

    updateVoteScreen: function() {
        $('h1').html(jsApp.currentQuestion.content)

        switch(jsApp.currentQuestion.phase)
        {
            case 'start':
                $('#answers').hide();
                $('#answered').hide();
                $('.message').hide();
                $('h1').show().html('De Mixapp wordt geactiveerd bij aanvang van het debat.');
                $('#intro').show();
                $('#header h2').html('Welkom').show();
                $('#header').removeClass('red');
                $('#arrow').attr('src', '/images/arrow_down.png');
                break;
            case 'vote':
                if(typeof(jsApp.currentQuestion.myAnswer) != 'undefined')
                {
                    $('#follow').hide();
                    jsApp.showAnswer();
                }
                else
                {
                    $('h1').show();
                    $('h2').show();
                    $('.message').hide();
                    $('#intro').hide();
                    $('#answers').show();
                    $('#answered').hide();
                    $('#header').removeClass('red');
                    $('#header h2').html('Je kan nu stemmen:');
                    $('#arrow').attr('src', '/images/arrow_down.png');
                }
                break;
            case 'wait':
                jsApp.showAnswer();
                break;
            case 'result':
                jsApp.showAnswer();
                break;
            case 'end':
                $('h1').hide();
                $('h2').show();
                $('#header h2').html('Klaar!');
                $('.message').hide();
                $('#intro').hide();
                $('#answers').hide();
                $('#answered').hide();
                $('#end').show();
                break;
        }
    },

    showAnswer: function () {
        $('#answers').hide();
        $('#intro').hide();
        $('h1').html('Volg nu het debat. De volgende vraag krijg je zo te zien.').show();
        $('h2').show();
        $('#answered').show();
        $('#header').addClass('red');
        $('#header h2').html('Bedankt voor je stem!');
        $('#noAnswer').hide();
        $('#arrow').attr('src', '/images/arrow_down_red.png');
        $('.answer').hide();
        if(typeof(jsApp.currentQuestion.myAnswer) == 'undefined')
        {
            $('#noAnswer').show();
        }
        else
        {
            $('#answer_' + jsApp.currentQuestion.myAnswer).show();
        }
    },

    initPresentation: function () {
        jsApp.updatePresentationScreen();
        setInterval(jsApp.fetchCurrent, 1000);
    },

    updatePresentationScreen: function() {
        $('#question').html(jsApp.currentQuestion.content)

        switch(jsApp.currentQuestion.phase)
        {
            case 'start':
                $('#end').hide();
                $('#logo').hide();
                $('#answered').hide();
                $('#chart').hide();
                $('#intro').show();
                $('#question').hide();
                $('h2').hide();
                $('#legend').hide();
                $('#message').show();
                break;
            case 'vote':
                $('#end').hide();
                $('#logo').show();
                $('#chart').hide();
                $('#answered').show();
                $('#intro').hide();
                $('#question').show();
                $('h2').hide();
                $('#legend').hide();
                $('#message').html('Ga naar mixapp.be om deel te nemen aan de stemming').show();
                $('#question').removeClass('result');
                break;
            case 'wait':
                $('#end').hide();
                $('#logo').show();
                $('#chart').hide();
                $('#answered').hide();
                $('#intro').hide();
                $('h2').hide();
                $('#legend').hide();
                $('#message').hide();
                $('#question').addClass('result');
                break;
            case 'result':
                $('#end').hide();
                $('#logo').show();
                $('#message').hide();
                $('#intro').hide();
                $('#answered').hide();
                $('h2').show();
                $('#question').addClass('result');
                jsApp.drawGraph();
                break;
            case 'end':
                $('#chart').hide();
                $('h2').hide();
                $('#logo').show();
                $('#answered').hide();
                $('#legend').hide();
                $('#intro').hide();
                $('#message').hide();
                $('h1').removeClass('result').hide();
                $('#end').show();
                break;
        }
    },

    drawGraph: function () {
        var chartOptions =  {
            animationEasing: "easeOutQuad",
            scaleShowLabels : false,
            scaleShowGridLines : true,
            barValueSpacing : 50,
            scaleLineWidth : 0,
            scaleGridLineWidth : 1,
            scaleGridLineColor : "rgba(255,255,255,.05)",
            scaleLineColor : "rgba(255,255,255,.05)",
            scaleOverride: true,
            scaleSteps : 10,
            scaleStepWidth : 10,
            scaleStartValue : 0,
            barDatasetSpacing : 50
        };

        $('#agreeResult').html(jsApp.currentQuestion.percentage_agree + '%');
        $('#disagreeResult').html(jsApp.currentQuestion.percentage_disagree + '%');
        $('#neutralResult').html(jsApp.currentQuestion.percentage_neutral + '%');

        $('#canvas').remove();
        $('#chart').css({
            'height': '500px',
            'width': '600px'
        }).show().prepend('<canvas id="canvas" width="600px" height="500px"></canvas>');
        var chartData = {
            labels : [''],
            datasets: [
                {
                    // agree
                    data : [jsApp.currentQuestion.percentage_agree],
                    fillColor:"#45bd7c"
                },
                {
                    // neutral
                    data : [jsApp.currentQuestion.percentage_neutral],
                    fillColor:"#f37336"
                },
                {
                    // disagree
                    data : [jsApp.currentQuestion.percentage_disagree],
                    fillColor:"#ee4754"
                }
            ]

        };

        var myChart = new Chart(document.getElementById("canvas").getContext("2d")).Bar(chartData, chartOptions);

        $('#legend').show();
    }
}
$(jsApp.init);